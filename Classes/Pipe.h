#ifndef __PIPE_H__
#define __PIPE_H__

#include "cocos2d.h"

class Pipe : public cocos2d::Scene
{
public:
	Pipe();
	void SpawnPipe(cocos2d::Scene* layer);
private:
	cocos2d::Size visibleSize;
	cocos2d::Vec2 origin;
};

#endif
