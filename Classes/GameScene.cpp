#include "GameScene.h"
#include "GameOverScene.h"
#include "Definitions.h" 
#include "audio/include/AudioEngine.h"


USING_NS_CC;

Scene* GameScene::createScene()
{
	auto scene = Scene::createWithPhysics();
	scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	scene->getPhysicsWorld()->setGravity(Vect(0, 0));

	auto layer = GameScene::create();
	layer->SetPhysicsWorld(scene->getPhysicsWorld());

	scene->addChild(layer);

	return scene;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in GameSceneScene.cpp\n");
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto backgroundSprite = Sprite::create("iphone/Background.png");
	backgroundSprite->setPosition(
		Vec2(
			visibleSize.width / 2 + origin.x,
			visibleSize.height / 2 + origin.y
		)
	);
	this->addChild(backgroundSprite);

	auto edgeBody = PhysicsBody::createEdgeBox(visibleSize, PHYSICSBODY_MATERIAL_DEFAULT, 3);
	edgeBody->setCollisionBitmask(OBSTACLE_COLLISION_BITMASK);
	edgeBody->setContactTestBitmask(false);

	auto edgeNode = Node::create();
	edgeNode->setPosition(
		Vec2(
			visibleSize.width / 2 + origin.x,
			visibleSize.height / 2 + origin.y
		)
	);
	edgeNode->setPhysicsBody(edgeBody);

	this->addChild(edgeNode);

	this->schedule(CC_SCHEDULE_SELECTOR(GameScene::SpawnPipe), PIPE_SPAWN_FREQUENCY * visibleSize.width);

	bird = new Bird(this);

	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(GameScene::onContactBegin, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);

	score = 0;

	TTFConfig labelConfig;
	labelConfig.fontFilePath = "fonts/Marker Felt.ttf";
	labelConfig.fontSize = SCORE_FONT_SIZE;
	labelConfig.glyphs = GlyphCollection::DYNAMIC;
	labelConfig.outlineSize = 2;
	labelConfig.customGlyphs = nullptr;
	labelConfig.distanceFieldEnabled = false;
	scoreLabel = Label::createWithTTF(
		labelConfig,
		std::to_string(score),
		TextHAlignment::CENTER,
		visibleSize.height * SCORE_FONT_SIZE
	);
	scoreLabel->setColor(Color3B::WHITE);
	scoreLabel->enableShadow(Color4B::BLACK);
	scoreLabel->enableOutline(Color4B::BLACK, 1);
	// position the label on the center of the screen
	scoreLabel->setPosition(
		Vec2(
			visibleSize.width / 2 + origin.x,
			visibleSize.height * 0.75 + origin.y
		)
	);
	// add the label as a child to this layer
	this->addChild(scoreLabel, 10000);


	// calls GameScene::update 
	this->scheduleUpdate();

	return true;
}


void GameScene::menuCloseCallback(Ref* pSender)
{
	//Close the cocos2d-x game scene and quit the application
	Director::getInstance()->end();

	/*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);


}

void GameScene::SpawnPipe(float dt)
{
	pipe.SpawnPipe(this);
}

bool GameScene::onContactBegin(cocos2d::PhysicsContact& contact)
{
	CCLOG("COLLISION  DETECTED");

	PhysicsBody* a = contact.getShapeA()->getBody();
	PhysicsBody* b = contact.getShapeB()->getBody();

	CCLOG("A %i && B %i", a->getCollisionBitmask(), b->getCollisionBitmask());

	CCLOG("BIRD_COLLISION_BITMASK  %d", BIRD_COLLISION_BITMASK);
	CCLOG("OBSTACLE_COLLISION_BITMASK  %d", OBSTACLE_COLLISION_BITMASK);

	// https://www.iforce2d.net/b2dtut/collision-filtering && https://www.codeandweb.com/physicseditor/tutorials/creating-physics-shapes-for-cocos2d-x
	bool isCollidedPipe =
		(BIRD_COLLISION_BITMASK == a->getCollisionBitmask() && OBSTACLE_COLLISION_BITMASK == b->getCollisionBitmask()) ||
		(BIRD_COLLISION_BITMASK == b->getCollisionBitmask() && OBSTACLE_COLLISION_BITMASK == a->getCollisionBitmask());
	bool isPointEarned =
		(BIRD_COLLISION_BITMASK == a->getCollisionBitmask() && POINT_COLLISION_BITMASK == b->getCollisionBitmask()) ||
		(BIRD_COLLISION_BITMASK == b->getCollisionBitmask() && POINT_COLLISION_BITMASK == a->getCollisionBitmask());
	if (true == isCollidedPipe) {
		CCLOG("BIRD COLLIDE WITH PIPE! SCORES EARNED: %i", score);
		AudioEngine::play2d("Sounds/Hit.mp3");

		Director::getInstance()->replaceScene(
			TransitionFade::create(TRANSITION_TIME, GameOverScene::createScene(score))
		);
	}
	else if (true == isPointEarned) {
		AudioEngine::play2d("Sounds/Point.mp3");
		score++;
		scoreLabel->setString(std::to_string(score));
	}

	return true;
}

bool GameScene::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event)
{
	CCLOG("GameScene::onTouchBegan");

	bird->Fly();

	this->scheduleOnce(CC_SCHEDULE_SELECTOR(GameScene::StopFlying), BIRD_FLY_DURATION);

	return true;
}

void GameScene::StopFlying(float dt)
{
	bird->StopFlying();
}

void GameScene::update(float dt)
{
	bird->Fall();
}
