#include "GameOverScene.h"
#include "GameScene.h"
#include "MainMenuScene.h"
#include "Definitions.h"

USING_NS_CC;

unsigned int thisSessionScore;

Scene* GameOverScene::createScene(unsigned int tempScore)
{
	thisSessionScore = tempScore;

	return GameOverScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in GameOverSceneScene.cpp\n");
}

// on "init" you need to initialize your instance
bool GameOverScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto backgroundSprite = Sprite::create("iphone/Background.png");
	backgroundSprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	this->addChild(backgroundSprite);

	auto retryItem = MenuItemImage::create(
		"iphone/Retry Button.png",
		"iphone/Retry Button Clicked.png",
		CC_CALLBACK_1(GameOverScene::GoToGameScene, this)
	);
	retryItem->setPosition(
		Vec2(
			visibleSize.width / 2 + origin.x,
			visibleSize.height / 4 * 3
		)
	);
	auto mainMenuItem = MenuItemImage::create(
		"iphone/Menu Button.png",
		"iphone/Menu Button Clicked.png",
		CC_CALLBACK_1(GameOverScene::GoToMainMenuScene, this)
	);
	mainMenuItem->setPosition(
		Vec2(
			visibleSize.width / 2 + origin.x,
			visibleSize.height / 4
		)
	);

	auto menu = Menu::create(retryItem, mainMenuItem, NULL);
	menu->setPosition(Vec2::ZERO);

	this->addChild(menu);

	UserDefault* def = UserDefault::getInstance();
	auto highScore = def->getIntegerForKey("HIGHSCORE", 0);

	if (thisSessionScore > highScore) {
		highScore = thisSessionScore;

		def->setIntegerForKey("HIGHSCORE", highScore);
	}

	def->flush();

	TTFConfig labelConfig;
	labelConfig.fontFilePath = "fonts/Marker Felt.ttf";
	labelConfig.fontSize = SCORE_FONT_SIZE;
	labelConfig.glyphs = GlyphCollection::DYNAMIC;
	labelConfig.outlineSize = 2;
	labelConfig.customGlyphs = nullptr;
	labelConfig.distanceFieldEnabled = false;
	thisSessionScoreLabel = Label::createWithTTF(
		labelConfig,
		std::to_string(thisSessionScore),
		TextHAlignment::CENTER,
		visibleSize.height * SCORE_FONT_SIZE
	);
	thisSessionScoreLabel->setColor(Color3B::WHITE);
	thisSessionScoreLabel->enableShadow(Color4B::BLACK);
	thisSessionScoreLabel->enableOutline(Color4B::BLACK, 1);
	// position the label on the center of the screen
	thisSessionScoreLabel->setPosition(
		Vec2(
			visibleSize.width * 0.25 + origin.x,
			visibleSize.height / 2 + origin.y
		)
	);
	// add the label as a child to this layer
	this->addChild(thisSessionScoreLabel, 10000);

	auto highScoreLabel = Label::createWithTTF(
		labelConfig,
		std::to_string(highScore),
		TextHAlignment::CENTER,
		visibleSize.height * SCORE_FONT_SIZE
	);
	highScoreLabel->setColor(Color3B::YELLOW);
	highScoreLabel->enableShadow(Color4B::BLACK);
	highScoreLabel->enableOutline(Color4B::BLACK, 1);
	// position the label on the center of the screen
	highScoreLabel->setPosition(
		Vec2(
			visibleSize.width * 0.75 + origin.x,
			visibleSize.height / 2 + origin.y
		)
	);
	// add the label as a child to this layer
	this->addChild(highScoreLabel, 10000);

	return true;
}


void GameOverScene::menuCloseCallback(Ref* pSender)
{
	//Close the cocos2d-x game scene and quit the application
	Director::getInstance()->end();

	/*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);


}

void GameOverScene::GoToMainMenuScene(cocos2d::Ref* sender)
{
	auto scene = MainMenuScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}

void GameOverScene::GoToGameScene(cocos2d::Ref* sender)
{
	auto scene = GameScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}
