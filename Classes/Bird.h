#ifndef __BIRD_H__
#define __BIRD_H__

#include "cocos2d.h"
#include "AudioEngine.h"

class Bird : public cocos2d::Scene
{
public:
	Bird(cocos2d::Scene* scene);
	void Fall();
	void Fly() {
		cocos2d::AudioEngine::play2d("Sounds/Wing.mp3");
		isFalling = false;
	};
	void StopFlying() { isFalling = true; };
private:
	cocos2d::Size visibleSize;
	cocos2d::Vec2 origin;
	cocos2d::Sprite* flappyBird;
	bool isFalling;
};

#endif
